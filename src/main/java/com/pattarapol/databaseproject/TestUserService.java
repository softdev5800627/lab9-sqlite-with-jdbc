/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject;

import com.pattarapol.databaseproject.model.User;
import com.pattarapol.databaseproject.service.UserService;

/**
 *
 * @author ASUS
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("user1", "password");
        if(user!=null){
            System.out.println("Welcome user : " + user.getName());
        }else{
            System.out.println("Error" );
        }
    }
}
